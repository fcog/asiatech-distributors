<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'asiatech_wp');

/** MySQL database username */
define('DB_USER', 'asiatech_wp');

/** MySQL database password */
define('DB_PASSWORD', 'JoYr&6#3l');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#]cl57$d,C zr}0:UQB~O.J%fT46!*QqJQzKAWsRCM,ZSZ0kA/OC:-/]^KYr.z#S');
define('SECURE_AUTH_KEY',  'bIpgkDA(>4I9Tr^~}YBzr*5i+J%78F;1B1Os5Q_-Wnwp!k1;!E+ClNHyp$ikKUI/');
define('LOGGED_IN_KEY',    'e6].F=XAf10t8B1Ji51-+aO@ Foqw@s=AD,6BC]t[f{_6X3jY}a8*NDByet@M,dw');
define('NONCE_KEY',        '[`aq<W7W`1}lM-+P2 Z{*,dyLV`Tos~!3L98l8!aE3[(^aL87HJjFCv#s{2c{g#_');
define('AUTH_SALT',        'L=A@uy|7Rsr&8)(&DppvX4)tox{2|5G2FbdiQoKB }!ZN<?&bG^0%kJ;Vodl4n^1');
define('SECURE_AUTH_SALT', '+5zDL+d]1Od&Lc^#$sqUgU*kVZgXsCZV3TcK+VWZ6%hbiH]$3z}qc<1}ufP)}Io&');
define('LOGGED_IN_SALT',   '[gU264-ky)k#HjHC6aStnXfWFY.2]?C6avt#/OQg?as|!?5+k;#zft`,3 lzL]xb');
define('NONCE_SALT',       'd_s*?P>[NesdQ.63(&xh_T|7 -z~L1=P%KQ6=nl7UFgC]?s4t4.!6,l9omM0]3[7');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'asia_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'es_ES');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
