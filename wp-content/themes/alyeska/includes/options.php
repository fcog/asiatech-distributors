<?php
/**
 * Use Options API to add options onto options already 
 * present in framework. This is possible in Theme Blvd 
 * Framework 2.1.0+.
 */

if( ! function_exists( 'alyeska_options' ) ) {
	function alyeska_options() {
		
		// Add Styles
		themeblvd_add_option_tab( 'styles', __( 'Styles', TB_GETTEXT_DOMAIN ), true );
		
		// Add Styles > Main section
		$main_options = array(
			array(	
				'name' 		=> __( 'Body Style', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Choose how you want the entire shape of your website to be set.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'body_style',
				'std'		=> 'light',
				'type' 		=> 'select',
				'options'	=> array(
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN )
				)
			),							
			array(	
				'name' 		=> __( 'Body Shape', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Choose how you want the entire shape of your website to be set.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'body_shape',
				'std'		=> 'boxed',
				'type' 		=> 'select',
				'options'	=> array(
					'boxed' 		=> __( 'Boxed', TB_GETTEXT_DOMAIN ),
					'stretch' 		=> __( 'Stretch', TB_GETTEXT_DOMAIN )
				)
			),		
			array(
				'name' 		=> __( 'Skin Color', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Choose the color you\'d like to use for the outer parts of your website.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'skin_color',
				'std'		=> 'blue',
				'type' 		=> 'select',
				'options'	=> array(
					'black' 		=> __( 'Black', TB_GETTEXT_DOMAIN ),
					'blue' 			=> __( 'Blue', TB_GETTEXT_DOMAIN ),
					'brown' 		=> __( 'Brown', TB_GETTEXT_DOMAIN ),
					'dark_purple'	=> __( 'Dark Purple', TB_GETTEXT_DOMAIN ),
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN ),
					'green' 		=> __( 'Green', TB_GETTEXT_DOMAIN ),
					'light_blue' 	=> __( 'Light Blue', TB_GETTEXT_DOMAIN ),
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'navy' 			=> __( 'Navy', TB_GETTEXT_DOMAIN ),
					'orange' 		=> __( 'Orange', TB_GETTEXT_DOMAIN ),
					'pink' 			=> __( 'Pink', TB_GETTEXT_DOMAIN ),
					'purple' 		=> __( 'Purple', TB_GETTEXT_DOMAIN ),
					'red' 			=> __( 'Red', TB_GETTEXT_DOMAIN ),
					'slate' 		=> __( 'Slate Grey', TB_GETTEXT_DOMAIN ),
					'teal' 			=> __( 'Teal', TB_GETTEXT_DOMAIN )
				)
			),							
			array(
				'name' 		=> __( 'Skin Texture', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Choose the texture you\'d like applied with the skin color you chose in the previous option.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'skin_texture',
				'std'		=> 'glass',
				'type' 		=> 'select',
				'options'	=> array(
					'bokeh' 		=> __( 'Bokeh', TB_GETTEXT_DOMAIN ),
					'cracked' 		=> __( 'Cracked', TB_GETTEXT_DOMAIN ),
					'diag' 			=> __( 'Diagonal Lines (Vintage)', TB_GETTEXT_DOMAIN ),
					'diag2' 		=> __( 'Diagonal Lines (Standard)', TB_GETTEXT_DOMAIN ),
					'dots' 			=> __( 'Dots', TB_GETTEXT_DOMAIN ),
					'glass' 		=> __( 'Glass', TB_GETTEXT_DOMAIN ),
					'glow' 			=> __( 'Glow', TB_GETTEXT_DOMAIN ),
					'grid' 			=> __( 'Grid', TB_GETTEXT_DOMAIN ),
					'grunge' 		=> __( 'Grunge', TB_GETTEXT_DOMAIN ),
					'horz' 			=> __( 'Horizontal Lines', TB_GETTEXT_DOMAIN ),
					'mosaic' 		=> __( 'Mosaic', TB_GETTEXT_DOMAIN ),
					'splatter' 		=> __( 'Splatter', TB_GETTEXT_DOMAIN ),
					'vert' 			=> __( 'Vertical Lines', TB_GETTEXT_DOMAIN ),
					'vintage' 		=> __( 'Vintage Wallpaper', TB_GETTEXT_DOMAIN ),
					'wood' 			=> __( 'Wood', TB_GETTEXT_DOMAIN ),
				)
			),		
			array(
				'name' 		=> __( 'Menu Color', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Choose the color you\'d like the main menu of your website to be.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'menu_color',
				'std'		=> 'dark',
				'type' 		=> 'select',
				'options'	=> array(
					'black' 		=> __( 'Black', TB_GETTEXT_DOMAIN ),
					'blue' 			=> __( 'Blue', TB_GETTEXT_DOMAIN ),
					'brown' 		=> __( 'Brown', TB_GETTEXT_DOMAIN ),
					'dark_purple'	=> __( 'Dark Purple', TB_GETTEXT_DOMAIN ),
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN ),
					'green' 		=> __( 'Green', TB_GETTEXT_DOMAIN ),
					'light_blue' 	=> __( 'Light Blue', TB_GETTEXT_DOMAIN ),
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'navy' 			=> __( 'Navy', TB_GETTEXT_DOMAIN ),
					'orange' 		=> __( 'Orange', TB_GETTEXT_DOMAIN ),
					'pink' 			=> __( 'Pink', TB_GETTEXT_DOMAIN ),
					'purple' 		=> __( 'Purple', TB_GETTEXT_DOMAIN ),
					'red' 			=> __( 'Red', TB_GETTEXT_DOMAIN ),
					'slate' 		=> __( 'Slate Grey', TB_GETTEXT_DOMAIN ),
					'teal' 			=> __( 'Teal', TB_GETTEXT_DOMAIN )
				)
			),							
			array(
				'name' 		=> __( 'Menu Shape', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Choose the shape you\'d like applied to the main menu of your website.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'menu_shape',
				'std'		=> 'flip',
				'type' 		=> 'select',
				'options'	=> array(
					'flip' 			=> __( 'Flip Over', TB_GETTEXT_DOMAIN ),
					'classic' 		=> __( 'Classic', TB_GETTEXT_DOMAIN )
				)
			),
										
			array(	
				'name' 		=> __( 'Menu Search Bar', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Choose whether you\'d like to show the popup search box on the main menu or not.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'menu_search',
				'std'		=> 'body_boxed',
				'type' 		=> 'select',
				'options'	=> array(
					'show' 			=> __( 'Show', TB_GETTEXT_DOMAIN ),
					'hide' 			=> __( 'Hide', TB_GETTEXT_DOMAIN )
				)
			),
			array(
				'name' 		=> __( 'Social Icon Color', TB_GETTEXT_DOMAIN ),
				'desc'		=> __( 'Select the color you\'d like applied to the social icons.', TB_GETTEXT_DOMAIN ),
				'id'		=> 'social_media_style',
				'std'		=> 'dark',
				'type' 		=> 'select',
				'options'	=> array(
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN ),
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'grey' 			=> __( 'Grey', TB_GETTEXT_DOMAIN )
				)
			)
		);
		themeblvd_add_option_section( 'styles', 'main_styles', __( 'Main', TB_GETTEXT_DOMAIN ), null, $main_options, false );

		// Add Styles > Links section
		$links_options = array(
			array( 
				'name' 		=> __( 'Link Color', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose the color you\'d like applied to links.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'link_color',
				'std' 		=> '#2a9ed4',
				'type' 		=> 'color'
			),
			array( 
				'name' 		=> __( 'Link Hover Color', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose the color you\'d like applied to links when they are hovered over.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'link_hover_color',
				'std' 		=> '#1a5a78',
				'type' 		=> 'color'
			)
		);
		themeblvd_add_option_section( 'styles', 'links', __( 'Links', TB_GETTEXT_DOMAIN ), null, $links_options, false );
		
		// Add Styles > Typography section
		$typography_options = array(
			array( 
				'name' 		=> __( 'Primary Font', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'This applies to most of the text on your site.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'typography_body',
				'std' 		=> array('size' => '12px','face' => 'lucida','color' => '', 'google' => ''),
				'atts'		=> array('size', 'face'),
				'type' 		=> 'typography'
			),
			array( 
				'name' 		=> __( 'Header Font', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'This applies to all of the primary headers throughout your site (h1, h2, h3, h4, h5, h6). This would include header tags used in redundant areas like widgets and the content of posts and pages.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'typography_header',
				'std' 		=> array('size' => '','face' => 'google','color' => '', 'google' => 'Yanone Kaffeesatz'),
				'atts'		=> array('face'),
				'type' 		=> 'typography'
			),
			array(
				'name' 		=> __( 'Special Font', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'It can be kind of overkill to select a super fancy font for the previous option, but here is where you can go crazy. There are a few special areas in this theme where this font will get used.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'typography_special',
				'std' 		=> array('size' => '','face' => 'google','color' => '', 'google' => 'Yanone Kaffeesatz'),
				'atts'		=> array('face'),
				'type' 		=> 'typography'
			)
		);
		themeblvd_add_option_section( 'styles', 'typography', __( 'Typography', TB_GETTEXT_DOMAIN ), null, $typography_options, false );
		
		// Add Styles > Custom section
		$custom_options = array(
			array( 
				'name' 		=> __( 'Custom CSS', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'If you have some minor CSS changes, you can put them here to override the theme\'s default styles. However, if you plan to make a lot of CSS changes, it would be best to create a child theme.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'custom_styles',
				'type'		=> 'textarea'
			)
		);
		themeblvd_add_option_section( 'styles', 'custom', __( 'Custom', TB_GETTEXT_DOMAIN ), null, $custom_options, false );

		// Add social media option to Layout > Header
		$social_media = array( 
			'name' 		=> __( 'Social Media Buttons', TB_GETTEXT_DOMAIN ),
			'desc' 		=> __( 'Configure the social media buttons you\'d like to show in the header of your site. Check the buttons you\'d like to use and then input the full URL you\'d like the button to link to in the corresponding text field that appears.<br><br>Example: http://twitter.com/jasonbobich<br><br><em>Note: On the "Email" button, if you want it to link to an actual email address, you would input it like this:<br><br><strong>mailto:you@youremail.com</strong></em><br><br><em>Note: If you\'re using the RSS button, your default RSS feed URL is:<br><br><strong>'.get_feed_link( 'feed' ).'</strong></em>', TB_GETTEXT_DOMAIN ),
			'id' 		=> 'social_media',
			'std' 		=> array( 
				'includes' =>  array( 'facebook', 'google', 'twitter', 'rss' ),
				'facebook' => 'http://facebook.com/jasonbobich',
				'google' => 'https://plus.google.com/116531311472104544767/posts',
				'twitter' => 'http://twitter.com/jasonbobich',
				'rss' => get_bloginfo('rss_url')
			),
			'type' 		=> 'social_media'
		);
		themeblvd_add_option( 'layout', 'header', 'social_media', $social_media );
		
		// Add header text option to Layout > Header
		$header_text = array( 
			'name' 		=> __( 'Header Text', TB_GETTEXT_DOMAIN ),
			'desc'		=> __( 'Enter a very brief piece of text you\'d like to show below the social icons.', TB_GETTEXT_DOMAIN ),
			'id'		=> 'header_text',
			'std'		=> '<strong>Call Now: 1-800-123-4567</strong>',
			'type' 		=> 'text'
		);
		themeblvd_add_option( 'layout', 'header', 'header_text', $header_text );
		
		// Add meta option for archive posts
		$archive_meta = array(
			'name' 		=> __( 'Show meta info?', TB_GETTEXT_DOMAIN ),
			'desc' 		=> __( 'Choose whether you want to show meta information under the title of each post.', TB_GETTEXT_DOMAIN ),
			'id' 		=> 'archive_meta',
			'std' 		=> 'show',
			'type' 		=> 'radio',
			'options' 	=> array(
				'show'	=> __( 'Show meta info.', TB_GETTEXT_DOMAIN ),
				'hide' 	=> __( 'Hide meta info.', TB_GETTEXT_DOMAIN )
			)
		);
		themeblvd_add_option( 'content', 'archives', 'archive_meta', $archive_meta );
		
		// Add tags option for archive posts
		$archive_meta = array(
			'name' 		=> __( 'Show tags?', TB_GETTEXT_DOMAIN ),
			'desc' 		=> __( 'Choose whether you want to show tags under at the bottom of each post.', TB_GETTEXT_DOMAIN ),
			'id' 		=> 'archive_tags',
			'std' 		=> 'show',
			'type' 		=> 'radio',
			'options' 	=> array(
				'show'	=> __( 'Show tags.', TB_GETTEXT_DOMAIN ),
				'hide' 	=> __( 'Hide tags.', TB_GETTEXT_DOMAIN )
			)
		);
		themeblvd_add_option( 'content', 'archives', 'archive_tags', $archive_meta );
		
		// Add comments link option for archive posts
		$archive_comment_link = array(
			'name' 		=> __( 'Show comments link?', TB_GETTEXT_DOMAIN ),
			'desc' 		=> __( 'Choose whether you want to show the comments link under at the bottom of each post. Keep in mind that if you want to disable the comment link for an individual post, you\'d do so under that post\'s discussion settings.', TB_GETTEXT_DOMAIN ),
			'id' 		=> 'archive_comment_link',
			'std' 		=> 'show',
			'type' 		=> 'radio',
			'options' 	=> array(
				'show'	=> __( 'Show comments link.', TB_GETTEXT_DOMAIN ),
				'hide' 	=> __( 'Hide comments link.', TB_GETTEXT_DOMAIN )
			)
		);
		themeblvd_add_option( 'content', 'archives', 'archive_comment_link', $archive_comment_link );
		
		// Add post list options 
		$post_list_description = __( 'These options apply to posts when they are shown from within any post list throughout your site. This includes the Primary Posts Display described above, as well.<br><br>Note: It may be confusing why these options are not present when editing a specific post list. The reason is because the options when working with a specific post list are incorporated into the actual theme framework, while these settings have been added to this particular theme design for your conveniance.', TB_GETTEXT_DOMAIN );
		$post_list = array(
			array(
				'name' 		=> __( 'Show meta info?', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose whether you want to show meta information under the title of each post.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'post_list_meta',
				'std' 		=> 'show',
				'type' 		=> 'radio',
				'options' 	=> array(
					'show'	=> __( 'Show meta info.', TB_GETTEXT_DOMAIN ),
					'hide' 	=> __( 'Hide meta info.', TB_GETTEXT_DOMAIN )
				)
			),
			array(
				'name' 		=> __( 'Show tags?', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose whether you want to show tags under at the bottom of each post.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'post_list_tags',
				'std' 		=> 'show',
				'type' 		=> 'radio',
				'options' 	=> array(
					'show'	=> __( 'Show tags.', TB_GETTEXT_DOMAIN ),
					'hide' 	=> __( 'Hide tags.', TB_GETTEXT_DOMAIN )
				)
			),
			array(
				'name' 		=> __( 'Show comments link?', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose whether you want to show the comment number in the top right corner of each post. Keep in mind that if you want to disable the comment link for an individual post, you\'d do so under that post\'s discussion settings.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'post_list_comment_link',
				'std' 		=> 'show',
				'type' 		=> 'radio',
				'options' 	=> array(
					'show'	=> __( 'Show comments link.', TB_GETTEXT_DOMAIN ),
					'hide' 	=> __( 'Hide comments link.', TB_GETTEXT_DOMAIN )
				)
			)
					
		);
		themeblvd_add_option_section( 'content', 'post_list', __( 'Post Lists', TB_GETTEXT_DOMAIN ), $post_list_description, $post_list );

		// Add post grid options 
		$post_grid_description = __( 'These options apply to posts when they are shown from within any post grid throughout your site.<br><br>Note: It may be confusing why these options are not present when editing a specific post grid. The reason is because the options when working with a specific post grid are incorporated into the actual theme framework, while these settings have been added to this particular theme design for your conveniance.', TB_GETTEXT_DOMAIN );
		$post_grid = array(
			array(
				'name' 		=> __( 'Show title?', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose whether or not you want to show the title below each featured image in post grids.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'post_grid_title',
				'std' 		=> 'show',
				'type' 		=> 'radio',
				'options' 	=> array(
					'show'	=> __( 'Show titles.', TB_GETTEXT_DOMAIN ),
					'hide' 	=> __( 'Hide titles.', TB_GETTEXT_DOMAIN )
				)
			),
			array(
				'name' 		=> __( 'Show excerpts?', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose whether or not you want to show the excerpt on each post.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'post_grid_excerpt',
				'std' 		=> 'show',
				'type' 		=> 'radio',
				'options' 	=> array(
					'show'	=> __( 'Show excerpts.', TB_GETTEXT_DOMAIN ),
					'hide' 	=> __( 'Hide excerpts.', TB_GETTEXT_DOMAIN )
				)
			),
			array(
				'name' 		=> __( 'Show buttons?', TB_GETTEXT_DOMAIN ),
				'desc' 		=> __( 'Choose whether or not you want to show a button that links to the single post.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'post_grid_button',
				'std' 		=> 'show',
				'type' 		=> 'radio',
				'options' 	=> array(
					'show'	=> __( 'Show buttons.', TB_GETTEXT_DOMAIN ),
					'hide' 	=> __( 'Hide buttons.', TB_GETTEXT_DOMAIN )
				)
			)
		);
		themeblvd_add_option_section( 'content', 'post_grid', __( 'Post Grids', TB_GETTEXT_DOMAIN ), $post_grid_description, $post_grid, false );
		
		// Modify framework options
		themeblvd_remove_option( 'config', 'responsiveness', 'mobile_nav' );
		themeblvd_edit_option( 'content', 'blog', 'blog_content', 'std', 'excerpt' );
	}
}
add_action( 'after_setup_theme', 'alyeska_options' );

/**
 * Setup theme for customizer.
 */
 
if( ! function_exists( 'alyeska_customizer' ) ) {
	function alyeska_customizer(){
		
		// Setup logo options
		$logo_options = array(
			'logo' => array( 
				'name' 		=> __( 'Logo', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'logo',
				'type' 		=> 'logo',
				'transport'	=> 'postMessage'
			)
		);
		themeblvd_add_customizer_section( 'logo', __( 'Logo', TB_GETTEXT_DOMAIN ), $logo_options, 1 );
		
		// Setup main styles options
		$main_style_options = array(
			'body_style' => array(	
				'name' 		=> __( 'Body Style', TB_GETTEXT_DOMAIN ),
				'id'		=> 'body_style',
				'type' 		=> 'select',
				'options'	=> array(
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN )
				),
				'priority' => 1
			),							
			'body_shape' => array(	
				'name' 		=> __( 'Body Shape', TB_GETTEXT_DOMAIN ),
				'id'		=> 'body_shape',
				'type' 		=> 'select',
				'options'	=> array(
					'boxed' 		=> __( 'Boxed', TB_GETTEXT_DOMAIN ),
					'stretch' 		=> __( 'Stretch', TB_GETTEXT_DOMAIN )
				),
				'priority' => 2
			),		
			'skin_color' => array(
				'name' 		=> __( 'Skin Color', TB_GETTEXT_DOMAIN ),
				'id'		=> 'skin_color',
				'type' 		=> 'select',
				'options'	=> array(
					'black' 		=> __( 'Black', TB_GETTEXT_DOMAIN ),
					'blue' 			=> __( 'Blue', TB_GETTEXT_DOMAIN ),
					'brown' 		=> __( 'Brown', TB_GETTEXT_DOMAIN ),
					'dark_purple'	=> __( 'Dark Purple', TB_GETTEXT_DOMAIN ),
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN ),
					'green' 		=> __( 'Green', TB_GETTEXT_DOMAIN ),
					'light_blue' 	=> __( 'Light Blue', TB_GETTEXT_DOMAIN ),
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'navy' 			=> __( 'Navy', TB_GETTEXT_DOMAIN ),
					'orange' 		=> __( 'Orange', TB_GETTEXT_DOMAIN ),
					'pink' 			=> __( 'Pink', TB_GETTEXT_DOMAIN ),
					'purple' 		=> __( 'Purple', TB_GETTEXT_DOMAIN ),
					'red' 			=> __( 'Red', TB_GETTEXT_DOMAIN ),
					'slate' 		=> __( 'Slate Grey', TB_GETTEXT_DOMAIN ),
					'teal' 			=> __( 'Teal', TB_GETTEXT_DOMAIN )
				),
				'priority' => 3
			),							
			'skin_texture' => array(
				'name' 		=> __( 'Skin Texture', TB_GETTEXT_DOMAIN ),
				'id'		=> 'skin_texture',
				'type' 		=> 'select',
				'options'	=> array(
					'bokeh' 		=> __( 'Bokeh', TB_GETTEXT_DOMAIN ),
					'cracked' 		=> __( 'Cracked', TB_GETTEXT_DOMAIN ),
					'diag' 			=> __( 'Diagonal Lines (Vintage)', TB_GETTEXT_DOMAIN ),
					'diag2' 		=> __( 'Diagonal Lines (Standard)', TB_GETTEXT_DOMAIN ),
					'dots' 			=> __( 'Dots', TB_GETTEXT_DOMAIN ),
					'glass' 		=> __( 'Glass', TB_GETTEXT_DOMAIN ),
					'glow' 			=> __( 'Glow', TB_GETTEXT_DOMAIN ),
					'grid' 			=> __( 'Grid', TB_GETTEXT_DOMAIN ),
					'grunge' 		=> __( 'Grunge', TB_GETTEXT_DOMAIN ),
					'horz' 			=> __( 'Horizontal Lines', TB_GETTEXT_DOMAIN ),
					'mosaic' 		=> __( 'Mosaic', TB_GETTEXT_DOMAIN ),
					'splatter' 		=> __( 'Splatter', TB_GETTEXT_DOMAIN ),
					'vert' 			=> __( 'Vertical Lines', TB_GETTEXT_DOMAIN ),
					'vintage' 		=> __( 'Vintage Wallpaper', TB_GETTEXT_DOMAIN ),
					'wood' 			=> __( 'Wood', TB_GETTEXT_DOMAIN ),
				),
				'priority' => 4
			),		
			'menu_color' => array(
				'name' 		=> __( 'Menu Color', TB_GETTEXT_DOMAIN ),
				'id'		=> 'menu_color',
				'type' 		=> 'select',
				'options'	=> array(
					'black' 		=> __( 'Black', TB_GETTEXT_DOMAIN ),
					'blue' 			=> __( 'Blue', TB_GETTEXT_DOMAIN ),
					'brown' 		=> __( 'Brown', TB_GETTEXT_DOMAIN ),
					'dark_purple'	=> __( 'Dark Purple', TB_GETTEXT_DOMAIN ),
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN ),
					'green' 		=> __( 'Green', TB_GETTEXT_DOMAIN ),
					'light_blue' 	=> __( 'Light Blue', TB_GETTEXT_DOMAIN ),
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'navy' 			=> __( 'Navy', TB_GETTEXT_DOMAIN ),
					'orange' 		=> __( 'Orange', TB_GETTEXT_DOMAIN ),
					'pink' 			=> __( 'Pink', TB_GETTEXT_DOMAIN ),
					'purple' 		=> __( 'Purple', TB_GETTEXT_DOMAIN ),
					'red' 			=> __( 'Red', TB_GETTEXT_DOMAIN ),
					'slate' 		=> __( 'Slate Grey', TB_GETTEXT_DOMAIN ),
					'teal' 			=> __( 'Teal', TB_GETTEXT_DOMAIN )
				),
				'priority' => 5
			),							
			'menu_shape' => array(
				'name' 		=> __( 'Menu Shape', TB_GETTEXT_DOMAIN ),
				'id'		=> 'menu_shape',
				'type' 		=> 'select',
				'options'	=> array(
					'flip' 			=> __( 'Flip Over', TB_GETTEXT_DOMAIN ),
					'classic' 		=> __( 'Classic', TB_GETTEXT_DOMAIN )
				),
				'priority' => 6
			),							
			'menu_search' => array(	
				'name' 		=> __( 'Menu Search Bar', TB_GETTEXT_DOMAIN ),
				'id'		=> 'menu_search',
				'type' 		=> 'select',
				'options'	=> array(
					'show' 			=> __( 'Show', TB_GETTEXT_DOMAIN ),
					'hide' 			=> __( 'Hide', TB_GETTEXT_DOMAIN )
				),
				'priority' => 7
			),
			'social_media_style' => array(
				'name' 		=> __( 'Social Icon Color', TB_GETTEXT_DOMAIN ),
				'id'		=> 'social_media_style',
				'type' 		=> 'select',
				'options'	=> array(
					'dark' 			=> __( 'Dark', TB_GETTEXT_DOMAIN ),
					'light' 		=> __( 'Light', TB_GETTEXT_DOMAIN ),
					'grey' 			=> __( 'Grey', TB_GETTEXT_DOMAIN )
				)
				,
				'priority' => 8
			)
		);
		themeblvd_add_customizer_section( 'main_styles', __( 'Main Styles', TB_GETTEXT_DOMAIN ), $main_style_options, 2 );
		
		// Setup primary font options
		$font_options = array(
			'typography_body' => array( 
				'name' 		=> __( 'Primary Font', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'typography_body',
				'atts'		=> array('size', 'face'),
				'type' 		=> 'typography',
				'transport'	=> 'postMessage'
			),
			'typography_header' => array( 
				'name' 		=> __( 'Header Font', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'typography_header',
				'atts'		=> array('face'),
				'type' 		=> 'typography',
				'transport'	=> 'postMessage'
			),
			'typography_special' => array(
				'name' 		=> __( 'Special Font', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'typography_special',
				'atts'		=> array('face'),
				'type' 		=> 'typography',
				'transport'	=> 'postMessage'
			)
		);
		themeblvd_add_customizer_section( 'typography', __( 'Typography', TB_GETTEXT_DOMAIN ), $font_options, 103 );
		
		$links_options = array(
			'link_color' => array( 
				'name' 		=> __( 'Link Color', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'link_color',
				'type' 		=> 'color'
			),
			'link_hover_color' => array( 
				'name' 		=> __( 'Link Hover Color', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'link_hover_color',
				'type' 		=> 'color'
			)
		);
		themeblvd_add_customizer_section( 'links', __( 'Links', TB_GETTEXT_DOMAIN ), $links_options, 103 );
		
		// Setup custom styles option
		$custom_styles_options = array(
			'custom_styles' => array( 
				'name' 		=> __( 'Enter styles to preview their results.', TB_GETTEXT_DOMAIN ),
				'id' 		=> 'custom_styles',
				'type' 		=> 'textarea',
				'transport'	=> 'postMessage'
			)
		);
		themeblvd_add_customizer_section( 'custom_css', __( 'Custom CSS', TB_GETTEXT_DOMAIN ), $custom_styles_options, 121 );
	}
}
add_action( 'after_setup_theme', 'alyeska_customizer' );

/**
 * Add specific theme elements to customizer.
 */

if( ! function_exists( 'alyeska_customizer_init' ) ) {
	function alyeska_customizer_init( $wp_customize ){
		
		// Remove custom background options
		$wp_customize->remove_section( 'colors' );
		$wp_customize->remove_section( 'background_image' );
		
		// Add real-time option edits
		if ( $wp_customize->is_preview() && ! is_admin() ){
			add_action( 'wp_footer', 'alyeska_customizer_preview', 21 );
		}
		
	}
}
add_action( 'customize_register', 'alyeska_customizer_init' );

/**
 * Add real-time option edits for this theme in customizer.
 */

if( ! function_exists( 'alyeska_customizer_preview' ) ) {
	function alyeska_customizer_preview(){
		
		// Global option name
		$option_name = themeblvd_get_option_name();
		
		// Begin output	
		?>
		<script type="text/javascript">
		window.onload = function(){ // window.onload for silly IE9 bug fix	
			(function($){
				
				// ---------------------------------------------------------
				// Logo
				// ---------------------------------------------------------
				
				<?php themeblvd_customizer_preview_logo(); ?>
				
				// ---------------------------------------------------------
				// Typography
				// ---------------------------------------------------------
				
				<?php themeblvd_customizer_preview_font_prep(); ?>
				<?php themeblvd_customizer_preview_primary_font(); ?>
				<?php themeblvd_customizer_preview_header_font(); ?>
				
				// ---------------------------------------------------------
				// Special Typography
				// ---------------------------------------------------------
				
				var special_font_selectors = '#branding .header_logo .tb-text-logo, #featured .media-full .slide-title, #content .media-full .slide-title, #featured_below .media-full .slide-title, .element-slogan .slogan .slogan-text, .element-tweet, .special-font';

				/* Special Typography - Face */
				wp.customize('<?php echo $option_name; ?>[typography_special][face]',function( value ) {
					value.bind(function(face) {
						if( face == 'google' ){
							googleFonts.specialToggle = true;
							var google_font = googleFonts.specialName.split(":"),
								google_font = google_font[0];
							$(special_font_selectors).css('font-family', google_font);
						}
						else
						{
							googleFonts.specialToggle = false;
							$(special_font_selectors).css('font-family', fontStacks[face]);
						}
					});
				});
				
				/* Special Typography - Google */
				wp.customize('<?php echo $option_name; ?>[typography_special][google]',function( value ) {
					value.bind(function(google_font) {
						// Only proceed if user has actually selected for 
						// a google font to show in previous option.
						if(googleFonts.specialToggle)
						{
							// Set global google font for reference in 
							// other options.
							googleFonts.specialName = google_font;
							
							// Remove previous google font to avoid clutter.
							$('.preview_google_special_font').remove();
							
							// Format font name for inclusion
							var include_google_font = google_font.replace(/ /g,'+');
							
							// Include font
							$('head').append('<link href="http://fonts.googleapis.com/css?family='+include_google_font+'" rel="stylesheet" type="text/css" class="preview_google_special_font" />');
							
							// Format for CSS
							google_font = google_font.split(":");
							google_font = google_font[0];
							
							// Apply font in CSS
							$(special_font_selectors).css('font-family', google_font);
						}
					});
				});
				
				// ---------------------------------------------------------
				// Custom CSS
				// ---------------------------------------------------------
				
				<?php themeblvd_customizer_preview_styles(); ?>
				
			})(jQuery);
		} // End window.onload for silly IE9 bug
		</script>
		<?php
	}
}