<?php
/*-----------------------------------------------------------------------------------*/
/* Theme Functions
/* 
/* In premium Theme Blvd themes, this file will contains everything that's needed 
/* to modify the framework's default setting to construct the current theme.
/*-----------------------------------------------------------------------------------*/

// Define theme constants
define( 'TB_THEME_ID', 'alyeska' );
define( 'TB_THEME_NAME', 'Alyeska' );

// Modify framework's theme options
require_once( get_template_directory() . '/includes/options.php' );

/**
 * Alyeska Setup 
 * 
 * You can override this function from a child 
 * theme if any basic setup things need to be changed.
 */

if( ! function_exists( 'alyeska_setup' ) ) {
	function alyeska_setup() {		
		// Custom background support
		global $wp_version;
		if ( version_compare( $wp_version, '3.4' ) >= 0 ) {
			// Add background support for WP 3.4+
			add_theme_support( 'custom-background' );
		} else {
			// Add background support for WP 3.0-3.3
			add_custom_background();
		}
	}
}
add_action( 'after_setup_theme', 'alyeska_setup' );

/**
 * Alyeska JS Files
 *
 * Here we're registering a JS file for the theme to 
 * replace the default one of the framework. Since 
 * we're registering here as "themeblvd," when it gets 
 * to registering it in the framework, it will skip 
 * over and just enequeue what we already have
 * registered here.
 */

if( ! function_exists( 'alyeska_js' ) ) {
	function alyeska_js() {
		wp_deregister_script( 'themeblvd' );
		wp_register_script( 'themeblvd', get_template_directory_uri() . '/assets/js/alyeska.min.js', array('jquery'), '1.0' );
	}
}
add_action( 'wp_enqueue_scripts', 'alyeska_js' );

/**
 * Alyeska CSS Files
 *
 * To add styles or remove unwanted styles that you 
 * know you won't need to maybe save some frontend load 
 * time, this function can easily be re-done from a 
 * child theme.
 */

if( ! function_exists( 'alyeska_css' ) ) {
	function alyeska_css() {
		if( ! is_admin() ) {
			wp_register_style( 'themeblvd_alyeska_nav', get_template_directory_uri() . '/assets/css/nav.min.css', false, '1.0' );
			wp_register_style( 'themeblvd_theme', get_template_directory_uri() . '/assets/css/theme.min.css', false, '1.0' );
			wp_register_style( 'themeblvd_alyeska_menu', get_template_directory_uri() . '/assets/css/menus.css', false, '1.0' );
			wp_register_style( 'themeblvd_alyeska_shape', get_template_directory_uri() . '/assets/css/shape/'.themeblvd_get_option( 'body_shape' ).'-'.themeblvd_get_option( 'body_style' ).'.min.css', false, '1.0' );
			wp_register_style( 'themeblvd_alyeska_style', get_template_directory_uri() . '/assets/css/style/'.themeblvd_get_option( 'body_style' ).'.min.css', false, '1.0' );
			wp_register_style( 'themeblvd_responsive', get_template_directory_uri() . '/assets/css/responsive.min.css', false, '1.0' );
			wp_register_style( 'themeblvd_ie', get_template_directory_uri() . '/assets/css/ie.css', false, '1.0' );
			wp_enqueue_style( 'themeblvd_alyeska_nav' );
			wp_enqueue_style( 'themeblvd_theme' );
			wp_enqueue_style( 'themeblvd_alyeska_menu' );
			wp_enqueue_style( 'themeblvd_alyeska_shape' );
			wp_enqueue_style( 'themeblvd_alyeska_style' );
			$GLOBALS['wp_styles']->add_data( 'themeblvd_ie', 'conditional', 'lt IE 9' ); // Add IE conditional
			wp_enqueue_style( 'themeblvd_ie' );
			if( themeblvd_get_option( 'responsive_css' ) != 'false' ) wp_enqueue_style( 'themeblvd_responsive' );
		}
	}
}
add_action( 'wp_print_styles', 'alyeska_css' );

/**
 * Alyeska Styles 
 * 
 * This is where the theme's configured styles 
 * from the Theme Options page get put into place 
 * by inserting CSS in the <head> of the site. It's 
 * shown here as clearly as possible to be edited, 
 * however it gets compressed when actually inserted 
 * into the front end of the site.
 */
if( ! function_exists( 'alyeska_styles' ) ) {
	function alyeska_styles() {
		$colors = array(
			'black' 		=> '#000000',
			'blue' 			=> '#0d306f',
			'brown' 		=> '#37261c',
			'dark_purple'	=> '#130323',
			'dark' 			=> '#383838',
			'green' 		=> '#254306',
			'light_blue' 	=> '#577f98',
			'light' 		=> '#ffffff',
			'navy' 			=> '#030b23',
			'orange' 		=> '#67250b',
			'pink' 			=> '#7a0951',
			'purple' 		=> '#745f7e',
			'red' 			=> '#510a0a',
			'slate' 		=> '#23282e',
			'teal' 			=> '#133735'
		);
		$skin_color = themeblvd_get_option( 'skin_color' );
		$custom_styles = themeblvd_get_option( 'custom_styles' );
		$body_font = themeblvd_get_option( 'typography_body' );
		$header_font = themeblvd_get_option( 'typography_header' );
		$special_font = themeblvd_get_option( 'typography_special' );
		themeblvd_include_google_fonts( $body_font, $header_font, $special_font );
		echo '<style>'."\n";
		ob_start();
		?>
		/* Skin */
		<?php if( ! get_background_image() && ! get_background_color() ) : ?>
		body {
			background: <?php echo $colors[$skin_color]; ?> url(<?php echo  get_template_directory_uri(); ?>/assets/images/layout/skin/footer-<?php echo $skin_color; ?>.png) 0 bottom repeat-x;
		}
		#wrapper {
			background: url(<?php echo  get_template_directory_uri(); ?>/assets/images/layout/skin/<?php echo themeblvd_get_option( 'skin_texture' ); ?>-<?php echo $skin_color; ?>.jpg) center 0 no-repeat;
		}
		<?php if( $skin_color == 'light' ) : ?>
		#branding .header_logo .tb-text-logo a,
		#branding .header_logo .tagline,
		#branding .header-text {
			color: #333333;
			text-shadow: 0 0 0 #fff;
		}
		<?php endif; ?>
		<?php endif; ?>
		/* Links */
		a {
			color: <?php echo themeblvd_get_option('link_color'); ?>;
		}
		a:hover,
		article .entry-title a:hover,
		.widget ul li a:hover,
		#breadcrumbs a:hover,
		.tags a:hover,
		.entry-meta a:hover,
		#footer_sub_content .copyright .menu li a:hover {
			color: <?php echo themeblvd_get_option('link_hover_color'); ?>;
		}
		/* Fonts */
		body {
			font-family: <?php echo themeblvd_get_font_face($body_font); ?>;
			font-size: <?php echo $body_font['size'];?>;
		}
		h1, h2, h3, h4, h5, h6, .slide-title {
			font-family: <?php echo themeblvd_get_font_face($header_font); ?>;
		}
		#branding .header_logo .tb-text-logo,
		#featured .media-full .slide-title,
		#content .media-full .slide-title,
		#featured_below .media-full .slide-title,
		.element-slogan .slogan .slogan-text,
		.element-tweet,
		.special-font {
			font-family: <?php echo themeblvd_get_font_face($special_font); ?>;
		}
		<?php if( themeblvd_get_option( 'responsive_css' ) == 'false' ) : ?>
		@media (max-width: 800px) {	
			#wrapper {
				min-width: 1020px; /* Force mobile and tablet browsers to zoom out. */
			}	
		}
		<?php endif; ?>
		<?php
		// Ouptput compressed CSS
		echo themeblvd_compress( ob_get_clean() );
		// Add in user's custom CSS
		if( $custom_styles ) echo $custom_styles;
		echo "\n</style>\n";
	}
}
add_action( 'wp_head', 'alyeska_styles' ); // Must come after framework loads styles, which are hooked with wp_print_styles

/*-----------------------------------------------------------------------------------*/
/* Add Sample Layout
/*
/* Here we add a sample layout to the layout builder's sample layouts.
/*-----------------------------------------------------------------------------------*/

$elements = array(
	array(
		'type' => 'slider',
		'location' => 'featured'
	),
	array(
		'type' => 'slogan',
		'location' => 'featured',
		'defaults' => array( 
			'slogan' => 'Your company rocks. This theme rocks. It\'s a perfect match.',
            'button' => 0,
            'button_text' => 'Get Started Today!',
            'button_color' => 'default',
            'button_url' => 'http://www.google.com',
            'button_target' => '_blank'
		)
	),
	array(
		'type' => 'columns',
		'location' => 'primary',
		'defaults' => array(
           'setup' => array(
				'num' => '3',
				'width' => array(
					'2' => 'grid_6-grid_6',
					'3' => 'grid_4-grid_4-grid_4',
					'4' => 'grid_3-grid_3-grid_3-grid_3',
					'5' => 'grid_fifth_1-grid_fifth_1-grid_fifth_1-grid_fifth_1-grid_fifth_1'
				)
			),
	        'col_1' => array(
				'type' => 'raw',
				'page' => null,
				'raw' => "<h2>It's Time For Alyeska</h2>\n[icon image=\"clock\" align=\"left\"]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\n[button link=\"#\"]Learn More[/button]",
			),
	        'col_2' => array(
				'type' => 'raw',
				'page' => null,
				'raw' => "<h2>Totally Responsive</h2>\n[icon image=\"mobile\" align=\"left\"]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\n[button link=\"#\"]Learn More[/button]",
			),
	        'col_3' => array(
				'type' => 'raw',
				'page' => null,
				'raw' => "<h2>It's Pixel Perfect</h2>\n[icon image=\"computer\" align=\"left\"]Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n\n[button link=\"#\"]Learn More[/button]"
			)
	    )
	)
);
themeblvd_add_sample_layout( 'alyeska', 'Alyeska Homepage', get_template_directory_uri().'/assets/images/sample-alyeska.png', 'full_width', $elements );

/*-----------------------------------------------------------------------------------*/
/* Theme Blvd Filters
/*
/* Here we can take advantage of modifying anything in the framework that is 
/* filterable. 
/*-----------------------------------------------------------------------------------*/

// Image Sizes
function alyeska_image_sizes( $sizes ) {
	$sizes['slider-large']['width'] = 940;
	$sizes['slider-large']['height'] = 350;
	$sizes['slider-staged']['width'] = 530;
	$sizes['slider-staged']['height'] = 312;
	return $sizes;
}
add_filter( 'themeblvd_image_sizes', 'alyeska_image_sizes' );

// Element CSS Classes
if( ! function_exists( 'alyeska_element_classes' ) ) {
	function alyeska_element_classes( $all_classes ) {
		$all_classes['element_columns'] = 'manual-gutters';
		$all_classes['element_content'] = 'boxed-layout';
		$all_classes['element_post_grid_paginated'] = 'boxed-layout';
		$all_classes['element_post_grid'] =	'boxed-layout';
		$all_classes['element_slogan'] = 'manual-gutters';
		$all_classes['element_tweet'] = 'boxed-layout';
		return $all_classes;
	}
}
add_filter( 'themeblvd_element_classes', 'alyeska_element_classes' );

// Theme Blvd WPML Bridge support
function alyeska_wpml_theme_locations( $current_locations ) {
	$new_locations = array();
	$new_locations['social_media_addon'] = array(
		'name' 		=> __( 'Social Media Addon', TB_GETTEXT_DOMAIN ),
		'desc' 		=> __( 'This will display your language flags next to your social icons in the header of your website.', TB_GETTEXT_DOMAIN ),
		'action' 	=> 'alyeska_header_wpml'
	);
	$new_locations = array_merge( $new_locations, $current_locations );
	return $new_locations;
}
add_filter( 'tb_wpml_theme_locations', 'alyeska_wpml_theme_locations' );

/*-----------------------------------------------------------------------------------*/
/* Theme Blvd Hooked Functions
/*
/* The following functions either add elements to unsed hooks in the framework, 
/* or replace default functions. These functions can be overridden from a child 
/* theme.
/*-----------------------------------------------------------------------------------*/

// Main Menu
if( ! function_exists( 'alyeska_header_menu' ) ) {
	function alyeska_header_menu() {
		?>
		<div id="menu-wrapper">
			<?php echo themeblvd_nav_menu_select( apply_filters( 'themeblvd_responsive_menu_location', 'primary' ) ); ?>
			<div id="main-top">
				<div class="main-top-left"></div>
				<div class="main-top-right"></div>
				<div class="main-top-middle"></div>
			</div>
			<div id="main-menu">
				<div id="menu-inner" class="<?php echo themeblvd_get_option( 'menu_shape' ); ?>-<?php echo themeblvd_get_option( 'menu_color' ); ?>">
					<div class="menu-left"><!-- --></div>
					<div class="menu-middle">
						<div class="menu-middle-inner">
							<?php wp_nav_menu( array('container' => '', 'theme_location' => 'primary', 'fallback_cb' => 'themeblvd_menu_fallback' ) ); ?>
							<?php do_action( 'themeblvd_header_menu_addon' ); ?>
							<?php if( themeblvd_get_option( 'menu_search' ) != 'hide') : ?>
								<div id="search-popup-wrapper">
									<a href="#" title="<?php echo themeblvd_get_local( 'search' ); ?>" id="search-trigger"><?php echo themeblvd_get_local( 'search' ); ?></a>
									<div class="search-popup-outer">
										<div class="search-popup">
										    <div class="search-popup-inner">
										        <form method="get" action="<?php bloginfo('url'); ?>">
										            <fieldset>
										                <input type="text" class="search-input" name="s" onblur="if (this.value == '') {this.value = '<?php echo themeblvd_get_local( 'search' ); ?>';}" onfocus="if(this.value == '<?php echo themeblvd_get_local( 'search' ); ?>') {this.value = '';}" value="<?php echo themeblvd_get_local( 'search' ); ?>" />
										                <input type="submit" class="submit" value="" />
										            </fieldset>
										        </form>
										    </div><!-- .search-popup-inner (end) -->
										</div><!-- .search-popup (end) -->
									</div><!-- .search-popup-outer (end) -->
								</div><!-- #search-popup-wrapper (end) -->
							<?php endif; ?>
						</div><!-- .menu-middle-inner (end) -->
					</div><!-- .menu-middle (end) -->
					<div class="menu-right"><!-- --></div>
				</div><!-- #menu-inner (end) -->
			</div><!-- #main-menu (end) -->
		</div><!-- #menu-wrapper (end) -->
		<?php
	}
}

// Header Addon
if( ! function_exists( 'alyeska_header_addon' ) ) {
	function alyeska_header_addon() {
		$header_text = themeblvd_get_option( 'header_text' );
		?>
		<div class="header-addon<?php if($header_text) echo ' header-addon-with-text'; if(has_action('alyeska_header_wpml')) echo ' header-addon-with-wpml';?>">
			<div class="social-media">
				<?php echo themeblvd_contact_bar(); ?>
			</div><!-- .social-media (end) -->
			<?php do_action('alyeska_header_wpml'); ?>
			<?php if( $header_text ) : ?>
				<div class="header-text">
					<?php echo $header_text; ?>
				</div><!-- .header-text (end) -->
			<?php endif; ?>
		</div><!-- .header-addon (end) -->
		<?php
	}
}

// Titles
if( ! function_exists( 'alyeska_content_top' ) ) {
	function alyeska_content_top() {
		global $post;
		?>
		<?php if( is_page() && ! is_page_template( 'template_builder.php' ) ) : ?>
			<?php if( 'hide' != get_post_meta( $post->ID, '_tb_title', true ) ) : ?>
				<header class="sidebar-layout-top entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->
			<?php endif; ?>
		<?php endif; ?>
		<?php if( is_archive() ) : ?>
			<?php if( 'true' == themeblvd_get_option( 'archive_title' ) ) : ?>
				<header class="sidebar-layout-top entry-header">
					<h1 class="entry-title"><?php themeblvd_archive_title(); ?></h1>
				</header><!-- .entry-header -->
			<?php endif; ?>
		<?php endif; ?>
		<?php
	}
}

// Blog Meta
if( ! function_exists( 'alyeska_blog_meta' ) ) {
	function alyeska_blog_meta() {
		?>
		<div class="entry-meta">
			<span class="author vcard"><?php _e( 'Posted by', TB_GETTEXT_DOMAIN ); ?> <a class="url fn n" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" title="<?php echo sprintf( esc_attr__( 'View all posts by %s', TB_GETTEXT_DOMAIN ), get_the_author() ); ?>" rel="author"><?php the_author(); ?></a></span> - 
			<time class="entry-date" datetime="<?php the_time('c'); ?>" pubdate><?php the_time( get_option('date_format') ); ?></time> - 
			<span class="category"><?php the_category(', '); ?></span>
		</div><!-- .entry-meta -->	
		<?php
	}
}

// Footer
if( ! function_exists( 'alyeska_footer_sub_content_default' ) ) {
	function alyeska_footer_sub_content_default() {
		?>
		<div id="footer_sub_content">
			<div class="container">
				<div class="content">
					<div class="copyright">
						<span class="text"><?php echo themeblvd_get_option( 'footer_copyright' ); ?></span>
						<span class="menu"><?php wp_nav_menu( array( 'menu_id' => 'footer-menu', 'container' => '', 'fallback_cb' => '', 'theme_location' => 'footer', 'depth' => 1 ) ); ?></span>
					</div><!-- .copyright (end) -->
					<div class="clear"></div>
				</div><!-- .content (end) -->
			</div><!-- .container (end) -->
		</div><!-- .footer_sub_content (end) -->
		<?php
	}
}

// Bottom of layout
if( ! function_exists( 'alyeska_footer_after' ) ) {
	function alyeska_footer_after() {
		?>
		<div id="after-footer">
			<div class="after-footer-left"></div>
			<div class="after-footer-right"></div>
			<div class="after-footer-middle"></div>
		</div>
		<?php
	}
}

/*-----------------------------------------------------------------------------------*/
/* Hook Adjustments on framework
/*-----------------------------------------------------------------------------------*/

// Remove Hooks
remove_action( 'themeblvd_header_menu', 'themeblvd_header_menu_default' );
remove_action( 'themeblvd_blog_meta', 'themeblvd_blog_meta_default' );
remove_action( 'themeblvd_footer_sub_content', 'themeblvd_footer_sub_content_default' );
remove_action( 'themeblvd_content_top', 'themeblvd_content_top_default');

// Add Hooks
add_action( 'themeblvd_header_addon', 'alyeska_header_addon' );
add_action( 'themeblvd_header_menu', 'alyeska_header_menu' );
add_action( 'themeblvd_content_top', 'alyeska_content_top' );
add_action( 'themeblvd_blog_meta', 'alyeska_blog_meta' );
add_action( 'themeblvd_footer_sub_content', 'alyeska_footer_sub_content_default' );
add_action( 'themeblvd_footer_after', 'alyeska_footer_after' );