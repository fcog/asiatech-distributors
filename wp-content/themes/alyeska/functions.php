<?php
/*-----------------------------------------------------------------------------------*/
/* Run Theme Blvd Framework
/* 
/* Below is the file needed to load the parent theme and theme framework. 
/* It's included with require_once(). 
/*
/* If you're creating a child theme, this line needs to be at the top of your 
/* child theme's functions.php. By doing this you're overriding the file being 
/* included here.
/*-----------------------------------------------------------------------------------*/

require_once ( get_template_directory() . '/framework/themeblvd.php' );

add_filter('user_trailingslashit', 'remcat_function');
function remcat_function($link) {

	//./equipos-nuevos/

    $link = str_replace("/category/", "/", $link);

    if ("/equipos-nuevos/" != $link && !strstr($link, 'usados')){
    	return str_replace("/equipos-nuevos/", "/", $link);
    }

    if ("/equipos-usados/" != $link && !strstr($link, 'nuevos')){
    	return str_replace("/equipos-usados/", "/", $link);
	}

    return $link;
}

function my_header_addon(){
    ?>
    <div id="language-box">
            <?php
            echo qtrans_generateLanguageSelectCode('both');
            ?>
    </div>
    <?php
}
add_action( 'themeblvd_header_addon', 'my_header_addon' );